---
title: "Restaurant d'europe"
author: "SASSI,ZIANI"
date: "3 avril 2018"
output: pdf_document
---



##--------------------------------BIG DATA AND STATISTICS--------------------------##


##------------SUMMARY -----------##
-Basic loaders
- What and why this data
- Data cleansing
- Basic plots
- Correlation
- Adcanved plot : rating / cuisine style
- Conclusion

Loading basic libraries , setting the correct working directory
```{r warning=FALSE, message=FALSE}
Sys.setenv(PATH=paste(Sys.getenv("PATH"),"C:/Program Files/MiKTeX 2.9/miktex/bin/x64/",sep=";"))
knitr::opts_chunk$set(echo = TRUE)

library(dplyr)
library(ggplot2)
library(magrittr)
library(scales)
library(tidyr)

options(scipen=999)  # turn-off scientific notation like 1e+48
theme_set(theme_bw())  # pre-set the bw theme.
```

```{r}
# Read CSV into R
dataRest <- read.csv(file="resto.csv", header=TRUE, sep=",")
```

## ----------------------------------------------- WHY AND WHAT ------------------------------------##
Restaurants in Europe
In this project we preferred to choose a data set which is interesting both for us and the assessors, and also a data set which is actual and have sense and today world, that is why we chose a dataset about restaurants in 31 most famous European cities 
This dataset has been obtained from TripAdvisor and therefore they obviously don't contain data reviews from unindexed restaurants.
Link to the dataset:
www.kaggle.com/damienbeneschi/krakow-ta-restaurans-data-raw


QUESTION:
The interrogation will be aproached in  a "customer perspective" and we rae going to ommit the price in order to facilitate the study: 

Which city / cuisine style is the bost avantageus for clients ? To give an answer , we need to know:

        For wich city and "cuisine style" we have the best Rating and ranking ratio ?
        Looking a city Is there a correlation between the  average rating  and the average ranking  ?



## ----------------------------------------------- DATA CLEANSING ------------------------------------##


We deleted 2 columns URL_TA ID_TA that can't be processed by the R software and thereby are irrelevant.

```{r}
dataRest %>% select(-URL_TA) -> dataRest;
dataRest %>% select(-ID_TA) -> dataRest;
```





With only 7.7% of NA values , these rows will be removed from the dataframe

```{r}
RateOfNaValues <- sum(is.na(dataRest$Rating))/length(dataRest$Rating);
RateOfNaValues;
```





The rating and ranking columns of the dataframe has NA values and therefore are unmaneagable
```{r}
dataRest %>% filter(!is.na(Rating)) -> dataRest;
dataRest %>% filter(!is.na(Ranking)) -> dataRest;
dataRest %>% filter(!is.na(Number.of.Reviews)) -> dataRest;
```





The data file has smmall issues with values of the rating columns (some 1's have '-' in it ) we need to correct that

```{r}
dataRest %>% filter(!is.na(Rating)) -> dataRest;
dataRest %>% mutate(Rating = abs(Rating)) -> dataRest;

```


## ----------------------------------------------- BASIC DETAILS AND PLOTS ------------------------------------##

Furthermore a group_by is performed to summarise data per city
```{r}
dataRest %>% group_by(City) %>% summarize(MoyRating = mean(Rating)) -> CityRating;
CityRating;
```



The first plot will show the avarage rating per city

```{r}
ggplot(CityRating, aes(x=City, y=MoyRating)) + 
  geom_bar(stat="identity", width=0.5, fill="tomato3") + 
  
 labs(title="Average rating per city", 
       subtitle="Plain view", 
       caption="source: me") + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6))

```
We notice that all values are in a close range

```{r}
ggplot(data=dataRest, aes(dataRest$Rating)) + 
  geom_histogram(stat="count", fill="tomato3") +
  labs(title="Rating dans spreadsheet") 

```


With 115,000 values we can go into details concerning the average rating per city

```{r}
ggplot(CityRating, aes(x=City, y=MoyRating)) + 
  geom_bar(stat="identity", width=0.5, fill="tomato3")  + 
  scale_y_continuous(limits=c(min(CityRating$MoyRating),max(CityRating$MoyRating)),oob = rescale_none) +
  labs(title="Average rating per city", 
       subtitle="Centered view in [min(rating) , max(rating)]", 
       caption="source: me") + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6))

```

So first element of answer : Athens is the best rated city for his restaurants
(Madrid though have some clear issues)

```{r}
CityRating %>% filter(MoyRating == max(MoyRating)) -> MaxCityRating;
MaxCityRating;
```



## --------------------------- CORRELATION BETWEEN RATING/RANKING POSSIBILITY  ------------------------------------##

To perform the plot we need the average rating and ranking per city .
```{r}
dataRest %>% group_by(City) %>% 
  summarise(MoyRating = mean(Rating) , MoyRanking = mean(Ranking) , SumReviews = sum(Number.of.Reviews)) -> moyRateRank;
head(moyRateRank)
```


The plot below will allow us to evaluate a possible correlation between Rating and Ranking
(I added SumReviews as size dots to give more clarity on who has more restaurants)

```{r}
ggplot(moyRateRank, aes(x=MoyRating, y=MoyRanking)) + 
  geom_point(aes(size=SumReviews), color="violetred2" ) +
  geom_smooth(method="loess", se=F) +

  labs(subtitle="Possible correlation Avg : Ranking / Rating", 
       y="Average Ranking", 
       x="Average Rating", 
       title="Ranking/Rating correlation", 
       caption = "Source: me")
```
Clearly there is no serious correlation between ranking and rating , see below for the example


In this plot I arbitrarily added some limitation to the geom_label in order to display only the intersting cities for our example (for i.e High Ranked Low Rated , Low Ranked High Rated)
```{r}
ggplot(moyRateRank, aes(x=MoyRating, y=MoyRanking)) + 
  geom_point(aes(size=SumReviews), color="violetred2" ) +
  geom_smooth(method="loess", se=F) +
  xlim(c(3.8, 4.25)) +  #limiting the plot to the interesting area of data
  ylim(c(0, 10000)) +
  geom_label(data= subset(moyRateRank, MoyRanking < 500 | MoyRanking > 7000 | MoyRating > 4.2 ) , aes(label=City),hjust=0.2, vjust=-0.3) +
  labs(subtitle="Quick naming of cities for previous uncorrelation", 
       y="Average Ranking", 
       x="Average Rating", 
       title="Ranking , Rating correlation", 
       caption = "Source: me")
```
Here above we have London : Average rating around 3.95 and ranking around 7600
                   Luxembourg : Average rating around 3.95 and ranking around 7600


Conclusion on correlation : globally there is'nt one ,but if we take the segment 4.0 -> 5.0 there is a little correlation




## ------------------- ANALYSING THE BEST CITIES / CUISINE STYLE ------------------------------------##


The original datasheet has summed up all the cuisine styles in a list (It was one list per row then)
To treat the data and allow operations like group-by , it's important to split it in different rows
```{r}
dataRest %>% 
  select(Cuisine.Style , Rating , Ranking)  %>% 
    mutate(Cuisine.Style = gsub("\\[|\\]|\\s", "" , dataRest$Cuisine.Style)) -> subedData #removing [ and]
separate_rows(subedData,Cuisine.Style,sep=",") -> styleData #spliting by ,
```



We perform a groub_by Cuisine.style
```{r}
styleData %>% group_by(Cuisine.Style) %>% 
  summarise(MoyRating = mean(Rating) , MoyRanking = mean(Ranking)) -> moyStyle
nrow(moyStyle)

moyStyle %>% arrange(desc(MoyRating)) -> moyStyle
moyStyle[1:10,] -> firstStyle 
moyStyle[117:127,] -> lastStyle
```
With 127 Different styles we are going to plot only the 1Oth best ones and the 10th worst (less good ones :)



```{r}
ggplot(firstStyle, aes(x=Cuisine.Style, y=MoyRating)) + 
  geom_bar(stat="identity", width=0.5, fill="tomato3")  + 
  scale_y_continuous(limits=c(min(firstStyle$MoyRating),max(firstStyle$MoyRating)),oob = rescale_none) +
  labs(title="Average rating per cuisine ", 
       subtitle="10 best cuisines, Centered view in [min(rating) , max(rating)]", 
       caption="source: me") + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6))

```


```{r}
ggplot(lastStyle, aes(x=Cuisine.Style, y=MoyRating)) + 
  geom_bar(stat="identity", width=0.5, fill="tomato3")  + 
  scale_y_continuous(limits=c(min(lastStyle$MoyRating),max(lastStyle$MoyRating)),oob = rescale_none) +
  labs(title="Average rating per cuisine", 
       subtitle="10 worst cuisines, Centered view in [min(rating) , max(rating)]", 
       caption="source: me") + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6))

```


The last step finally to give a complete answer to our initial question we are going to select the best cuisine and the best city combined to see the best rating
```{r}
dataRest %>% 
  select(Cuisine.Style , City ,  Rating , Ranking)  %>% 
    mutate(Cuisine.Style = gsub("\\[|\\]|\\s", "" , dataRest$Cuisine.Style)) -> subedData

separate_rows(subedData,Cuisine.Style,sep=",") -> subedDat
subedDat %>% group_by(City,Cuisine.Style) %>% summarise(MoyRating = mean(Rating) , MoyRanking = mean(Ranking)) -> perCityStyle
perCityStyle %>% arrange(desc(MoyRating)) -> perCityStyle
```


Here the 10 first ones
```{r}
head(perCityStyle , n= 100)
```


And the worst ones
```{r}
tail(perCityStyle , n= 10)
```



## --------------------------- CONCLUSION------------------------------------##

To conclude the study is mainly focused on relation ship between City - Style Cuisine - Rating
Because we demonstrated earlier that ther is'nt clear correlation between Restarurant Ranking and Rating

In a nutshell 
A restaurant in paris, doing Native American cuisine  is bad , but eating taiwanese in geneva could be a good choice !